package dk.topdanmark.api.transformers;

import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.processor.MessageProcessor;
import org.mule.module.apikit.exception.BadRequestException;

import dk.topdanmark.api.dto.DistributionRequest;
import dk.topdanmark.api.exceptions.QueueTimeoutException;

public class QueueProcessorMockup implements MessageProcessor {

	@Override
	public MuleEvent process(MuleEvent event) throws MuleException {
		DistributionRequest request = (DistributionRequest) event.getMessage().getPayload();
		if (null == request.getCustomerId())
			throw new BadRequestException("Customer cannot be null");
		if (request.getCustomerId() % 2 == 1) {
			throw new QueueTimeoutException();
		}
		return event;
	}

}
