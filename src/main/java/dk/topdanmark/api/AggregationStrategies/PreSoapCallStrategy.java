package dk.topdanmark.api.AggregationStrategies;

import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.routing.AggregationContext;
import org.mule.routing.AggregationStrategy;
import org.mule.routing.CollectAllAggregationStrategy;

import dk.topdanmark.api.builders.DistrubutionSoapRequestBuilder;
import dk.topdanmark.api.dto.Customer;
import dk.topdanmark.api.dto.DistibutionSoapRequest;
import dk.topdanmark.api.dto.DistributionRequest;
import dk.topdanmark.api.dto.Policy;
import dk.topdanmark.api.exceptions.GenericException;
import dk.topdanmark.api.exceptions.QueueTimeoutException;

public class PreSoapCallStrategy extends CollectAllAggregationStrategy implements AggregationStrategy {

	@Override
	public MuleEvent aggregate(AggregationContext context) throws MuleException {
		MuleEvent result = super.aggregate(context);

		checkForExceptions(context);

		processEventAndSetPayload(context, result);
		return result;
	}

	private void processEventAndSetPayload(AggregationContext context, MuleEvent result) throws GenericException {
		DistrubutionSoapRequestBuilder builder = new DistrubutionSoapRequestBuilder();

		for (MuleEvent event : context.collectEventsWithoutExceptions()) {
			Object payload = event.getMessage().getPayload();
			if (payload instanceof Policy)
				builder.setPolicy((Policy) payload);
			else if (payload instanceof Customer)
				builder.setCustomer((Customer) payload);
			else if (payload instanceof DistributionRequest)
				builder.setDistributionRequest((DistributionRequest) payload);
			else
				throw new GenericException("Invalid payload type", null);
		}
		DistibutionSoapRequest soapRequest = builder.build();
		result.getMessage().setPayload(soapRequest);
	}

	private void checkForExceptions(AggregationContext context) throws MuleException {
		if (context.collectRouteExceptions().size() > 0) {
			Throwable throwable = context.collectRouteExceptions().get(0);
			if (throwable instanceof QueueTimeoutException)
				throw (QueueTimeoutException) throwable;
			else if (throwable instanceof IllegalArgumentException)
				throw (IllegalArgumentException) throwable;
			else
				throw new GenericException("An error occurred", throwable);
		}
	}
}
