package dk.topdanmark.api.exceptions;

import org.mule.api.MuleException;

public class QueueTimeoutException extends MuleException {
	private static final long serialVersionUID = -1084665521741989718L;
}
