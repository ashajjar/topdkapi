package dk.topdanmark.api.exceptions;

import org.mule.api.MuleException;

public class GenericException extends MuleException {

	private static final long serialVersionUID = 8919053681405628835L;
	private Throwable cause;

	public GenericException(String message, Throwable cause) {
		this.cause = cause;
		this.setMessage(message);
	}
}
