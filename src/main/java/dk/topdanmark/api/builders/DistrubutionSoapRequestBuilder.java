package dk.topdanmark.api.builders;

import dk.topdanmark.api.dto.Customer;
import dk.topdanmark.api.dto.DistibutionSoapRequest;
import dk.topdanmark.api.dto.DistributionRequest;
import dk.topdanmark.api.dto.Policy;
import dk.topdanmark.api.exceptions.GenericException;

public class DistrubutionSoapRequestBuilder {
	private Policy policy;
	private Customer customer;
	private DistributionRequest distributionRequest;

	public DistrubutionSoapRequestBuilder setPolicy(Policy policy) {
		this.policy = policy;
		return this;
	}

	public DistrubutionSoapRequestBuilder setCustomer(Customer customer) {
		this.customer = customer;
		return this;
	}

	public DistrubutionSoapRequestBuilder setDistributionRequest(DistributionRequest distributionRequest) {
		this.distributionRequest = distributionRequest;
		return this;
	}

	public DistibutionSoapRequest build() throws GenericException {
		if (null == policy || null == customer || null == distributionRequest) {
			throw new GenericException("Policy and Customer must be set", null);
		}
		DistibutionSoapRequest soapRequest = new DistibutionSoapRequest();
		soapRequest.setCity(customer.getCity());
		soapRequest.setCountry(customer.getCountry());
		soapRequest.setCustomerId(distributionRequest.getCustomerId());
		soapRequest.setName(customer.getName());
		soapRequest.setPolicynumber(policy.getPolicy());
		soapRequest.setPolicytype(policy.getType());
		soapRequest.setPrice(policy.getPremium().toString() + " " + policy.getCurrency());
		soapRequest.setStreet_1(customer.getStreet_1());
		soapRequest.setStreet_2(customer.getStreet_2());
		soapRequest.setZipcode(customer.getZipcode());
		return soapRequest;
	}
}
