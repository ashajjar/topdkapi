package dk.topdanmark.api.dto;

public class Policy {

	private Long policy;
	private String type;
	private String active_from;
	private Double premium;
	private String currency;

	public Long getPolicy() {
		return policy;
	}

	public void setPolicy(Long policy) {
		this.policy = policy;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getActive_from() {
		return active_from;
	}

	public void setActive_from(String active_from) {
		this.active_from = active_from;
	}

	public Double getPremium() {
		return premium;
	}

	public void setPremium(Double premium) {
		this.premium = premium;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
