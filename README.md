The Process
The following are the steps followed to create this solution:
1-	Create a mockup of the external services:
a.	The SOAP Service: using SoapUI
b.	The CRM Queue: With mule and ActiveMQ Local Instance
c.	The Policy REST API: using mule studio by generating the flow from the provided RAML.
2-	Write the RAML file for the new API (challenge.raml) 
3-	Generate the mule flow from the RAML file
4-	Adjust the generated flow according to the challenge requirements.

Impediments

SOAP Service MockUp

The process would have been easier if the mockup war file of (SoapUI) was provided, because this took a lot of time trying to figure it out, in the end it turned out that the latest version of SoapUI has some issues with MacOS so an older version was used to get it to work.

Implementing the Flow

The flow was implemented smoothly, until the point where 503 (service unavailable) must be returned. The API Kit of Mule Studio does not support http status codes bigger than 500 (it seems like a global issue), after a lot of tweaking and researching, it looked like the whole exception catching strategy has to be changed to normal exception strategy instead of using the API Kit default. Then on the catch, a choice gate was used to decide what kind of error should be returned.

Technical Issues

Parallelism

Since Scatter-Gather processor will return a collection of messages that has as many as the number of its threads, the default aggregation strategy was overridden by the class PreSoapCallStrategy which will do the aggregation and some exception checks in case any thread has failed to process its work the aggregator will throw the proper exception, otherwise it will return a consolidated message payload.

Input Validation

Before sending the message to Scatter-Gather processor it will be validated and an exception will be thrown accordingly.

Testing

•	To test the case of queue unavailable odd customer id must be used, odd numbers will simulate an unavailable queue
•	To test the case of bad request (400 Status code), customer id and/or policy id has to be removed from the request payload.
•	To test happy path, policy id and even customer id must be provided and the flow must return 200

Potential Future Work

•	Exception handling logic needs some refactoring.
